from django.db import models
from django.contrib.auth.models import User


class Kategori(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Berita(models.Model):
    
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    content = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
    